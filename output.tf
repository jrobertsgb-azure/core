output "name" {
  value = "${azurerm_resource_group.test.name}"
}

output "location" {
  value = "${azurerm_resource_group.test.location}"
}

output "network" {
    value = "${azurerm_virtual_network.test.name}"
}

output "subnet" {
    value = "${azurerm_subnet.test.id}"
}

output "public_key" {
    value = "${tls_private_key.ssh.public_key_openssh}"
}

output "private_key" {
  value = "${tls_private_key.ssh.private_key_pem}"
  sensitive = true
}
